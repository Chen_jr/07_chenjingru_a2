﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }
        void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }


        [Test]
        public void RollGutterGame()
        {
            RollMany(20, 0);

            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOne()
        {
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }

        [Test]
        public void RollSpareFirstFrame()
        {
            game.Roll(9);
            game.Roll(1);
            RollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(28));
        }

        [Test]
        public void RollStrikeFirstFrame()
        {
            game.Roll(10);
            RollMany(19, 1);

            Assert.That(game.Score(), Is.EqualTo(30));
        }

        [Test]
        public void RollPerfectGame()
        {
            RollMany(20, 10);

            Assert.That(game.Score(), Is.EqualTo(300));
        }

        [Test]
        public void RollGame()
        {
            game.Roll(10);
            game.Roll(9);
            game.Roll(1);
            game.Roll(7);
            game.Roll(2);
            game.Roll(10);
            game.Roll(10);
            game.Roll(10);
            game.Roll(9);
            game.Roll(0);
            game.Roll(8);
            game.Roll(2);
            game.Roll(9);
            game.Roll(1);
            game.Roll(10);

            Assert.That(game.Score(), Is.EqualTo(187));
        }

        [Test]
        public void RollNineOneSpare()
        {
            RollMany(18, 10);
            game.Roll(1);
            game.Roll(1);

            Assert.That(game.Score(), Is.EqualTo(187));
        }

        [Test]
        public void RollOneAtFirst()
        {
            game.Roll(1);
            game.Roll(1);
            RollMany(18, 10);

            Assert.That(game.Score(), Is.EqualTo(187));
        }
    }
}
